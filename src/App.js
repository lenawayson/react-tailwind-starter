import React from 'react';
import logo from './logo.svg';

function App() {
  return (
    <div>
      <img src={logo} class="h-12 mx-auto" alt="logo" />
      <h1 class="text-4xl font-bold text-center text-blue-500">Hello World</h1>
    </div>
        
  );
}

export default App;
